import pylab as plt
import numpy as np
import numpy.random as rnd
from scipy.stats import poisson

'''
We will be simulating the following reaction:
    E + S <--> ES --> E + P
'''


class SimulationData:

    def __init__(self):
        ### Times ###
        self.dt = 0.02
        self.end_time = 60.0 * 3  # 3 minutes
        self.start_time = 0
        self.time = 0.0
        self.time_remainder = 0.0
        self.last_time = 0.0
        self.fixed_time = 0.0
        self.last_fixed_time = 0.0
        self.roll_over_boolean = False
        self.turnover_time = 0.0

        # S + E <--> ES --> P + E
        ### Rate Constants ###
        self.c1f = 1.0  # First reaction, forward
        self.c1r = 1.0  # First reaction, reverse
        self.cc = 1.0

        ### Populations ###
        self.pop_S = 10000
        self.pop_E = 10
        self.pop_ES = 0
        self.pop_P = 0

        ### Propensities ###
        self.a0 = 0.0
        self.a1f = 0.0
        self.a1r = 0.0
        self.ac = 0.0

        ### Data recording arrays ###
        self.record_time = []
        self.record_pop_S = []
        self.record_pop_E = []
        self.record_pop_ES = []
        self.record_pop_P = []
        self.record_turnover_time = []
        self.record_probability_density = []

        ### Other ###
        self.probability_density = 0.0

    def update_propensities(self):
        self.a1f = float(self.pop_E) * float(self.pop_S) * self.c1f  # E + S --> ES
        self.a1r = float(self.pop_ES) * self.c1r  # ES --> E + S
        self.ac = float(self.pop_ES) * self.cc  # ES --> E + P
        self.a0 = self.a1f + self.a1r + self.ac

    def react(self):
        p = self.a0 * rnd.random()

        ### Reaction 1, E + S --> ES ###
        if p <= self.a1f:
            self.pop_S -= 1
            self.pop_E -= 1
            self.pop_ES += 1
        ### Reaction 2, ES --> E + S ###
        elif self.a1f < p <= self.a1f + self.a1r:
            self.pop_S += 1
            self.pop_E += 1
            self.pop_ES -= 1
        ### Reaction 3, ES --> E + P ###
        else:
            self.pop_ES -= 1
            self.pop_E += 1
            self.pop_P += 1
        return

    def record_state(self):
        self.record_time.append(self.time)
        self.record_pop_S.append(self.pop_S)
        self.record_pop_E.append(self.pop_E)
        self.record_pop_ES.append(self.pop_ES)
        self.record_pop_P.append(self.pop_P)
        self.record_probability_density.append(self.probability_density)
        self.record_turnover_time.append(self.turnover_time)

    def display_results(self):
        plt.subplot(4, 1, 1)
        plt.title('E')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_E)

        plt.subplot(4, 1, 2)
        plt.title('ES')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_ES)

        plt.subplot(4, 1, 3)
        plt.title('P')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_P)

        plt.subplot(4, 1, 4)
        plt.title('S')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_S)

        plt.subplots_adjust(hspace=1)
        plt.show()

    def distributions(self):
        vec_a = np.array([self.a1f, self.a1r, self.ac])
        S = np.array([[-self.c1f, self.c1f], [self.c1r, -self.c1r - self.cc]])

        f = vec_a * np.exp((S * self.time) * (-S * [1,1]))

        return

if __name__ == '__main__':
    sd = SimulationData()
    sd.update_propensities()   ### Initialize propensities ###
    sd.record_state()  ### Record initial state ###

    while sd.time < sd.end_time:
        tau = (1 / sd.a0) * np.log(1 / rnd.random())

        sd.time += tau
        sd.time_remainder += tau % sd.dt

        ### The following if blocks determine how many fixed time outputs must be skipped over (if any). ###
        if sd.time_remainder > sd.dt:
            sd.roll_over_boolean = True
            sd.time_remainder -= sd.dt

        if sd.roll_over_boolean:
            for i in range(int(tau / sd.dt) + 1):
                sd.fixed_time += sd.dt
                sd.record_state()  # Record state before reaction.
            roll_over_boolean = False
        else:
            for j in range(int(tau / sd.dt)):
                sd.fixed_time += sd.dt
                sd.record_state()  # Record state before reaction.

        sd.react()
        sd.update_propensities()

    sd.record_state()  # Records final state of the system.
    sd.display_results()


