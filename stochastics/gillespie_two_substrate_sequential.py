import pylab as plt
import numpy as np
import numpy.random as rnd
from scipy.stats import poisson

'''
We will be simulating the following reaction:
    E + S1 <--> ES1
    ES1 + S2 <--> ES1S2
    ES1S2 --> E + S1 + S2
    ES1S2 --> E + P
'''

class SimulationData:

    def __init__(self):
        ### Times ###
        self.dt = 0.02
        self.end_time = 60.0 * 3  # 3 minutes
        self.start_time = 0
        self.time = 0.0
        self.time_remainder = 0.0
        self.last_time = 0.0
        self.fixed_time = 0.0
        self.last_fixed_time = 0.0
        self.roll_over_boolean = False

        # S + E <--> ES --> P + E
        ### Rate Constants ###
        self.c1f = 1.0  # Reaction 1 forward, E + S1 --> ES1
        self.c1r = 1.0  # Reaction 1 reverse, ES1 --> E + S1
        self.c2f = 1.0  # Reaction 2 forward, ES1 + S2 --> ES1S2
        self.c2r = 1.0  # Reaction 2 reverse, ES1S2 --> ES1 + S2
        self.c3f = 0.5  # Reaction 3 forward, ES1S2 --> E + P
        self.c3r = 0.01  # Reaction 3 reverse, ES1S2 --> E + S1 + S2

        ### Populations ###
        self.pop_S1 = 1000
        self.pop_S2 = 1000
        self.pop_E = 10
        self.pop_ES1 = 0
        self.pop_ES1S2 = 0
        self.pop_P = 0

        ### Propensities ###
        self.a0 = 0.0
        self.a1f = 0.0
        self.a1r = 0.0
        self.a2f = 0.0
        self.a2r = 0.0
        self.a3f = 0.0
        self.a3r = 0.0

        ### Data recording arrays ###
        self.record_time = []
        self.record_pop_S1 = []
        self.record_pop_S2 = []
        self.record_pop_E = []
        self.record_pop_ES1 = []
        self.record_pop_ES1S2 = []
        self.record_pop_P = []

    def update_propensities(self):
        self.a1f = float(self.pop_E) * float(self.pop_S1) * self.c1f  # E + S1 --> ES1
        self.a1r = float(self.pop_ES1) * self.c1r  # ES1 --> E + S1
        self.a2f = float(self.pop_ES1) * float(self.pop_S2) * self.c2f  # ES1 + S2 --> ES1S2
        self.a2r = float(self.pop_ES1S2) * self.c2r  # ES1S2 --> ES1 + S2
        self.a3r = float(self.pop_ES1S2) * self.c3f  # ES1S2 --> E + P
        self.a3f = float(self.pop_ES1S2) * self.c3r  # ES1S2 --> E + S1 + S2
        self.a0 = self.a1f + self.a1r + self.a2f + self.a2r + self.a3f + self.a3r

    def react(self):
        point = self.a0 * rnd.random()

        ### Reaction 1 forward, E + S1 --> ES1 ###
        if point <= self.a1f:
            self.pop_E -= 1
            self.pop_S1 -= 1
            self.pop_ES1 += 1
        ### Reaction 1 reverse, ES1 --> E + S1 ###
        elif self.a1f < point <= self.a1f + self.a1r:
            self.pop_E += 1
            self.pop_S1 += 1
            self.pop_ES1 -= 1
        #### Reaction 2 forward, ES1 + S2 --> ES1S2 ###
        elif self.a1f + self.a1r < point <= self.a1f + self.a1r + self.a2f:
            self.pop_ES1 -= 1
            self.pop_S2 -= 1
            self.pop_ES1S2 += 1
        #### Reaction 2 reverse, ES1S2 --> ES1 + S2 ###
        elif self.a1f + self.a1r + self.a2f < point <= self.a1f + self.a1r + self.a2f + self.a2r:
            self.pop_ES1 += 1
            self.pop_S2 += 1
            self.pop_ES1S2 -= 1
        #### Reaction 3 forward, ES1S2 --> E + P ###
        elif self.a1f + self.a1r + self.a2f + self.a2r < point <= self.a1f + self.a1r + self.a2f + self.a2r + self.a3f:
            self.pop_E += 1
            self.pop_ES1S2 -= 1
            self.pop_P += 1
        ### Reaction 3 reverse, ES1S2 --> E + S1 + S2 ###
        else:
            self.pop_E += 1
            self.pop_S1 += 1
            self.pop_S2 += 1
            self.pop_ES1S2 -= 1
        return

    def record_state(self):
        self.record_time.append(self.time)
        self.record_pop_S1.append(self.pop_S1)
        self.record_pop_S2.append(self.pop_S2)
        self.record_pop_E.append(self.pop_E)
        self.record_pop_ES1.append(self.pop_ES1)
        self.record_pop_ES1S2.append(self.pop_ES1S2)
        self.record_pop_P.append(self.pop_P)

    def display_results(self):
        plt.subplot(4, 2, 1)
        plt.title('S1')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_S1)

        plt.subplot(4, 2, 2)
        plt.title('ES2')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_S2)

        plt.subplot(4, 2, 3)
        plt.title('E')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_E)

        plt.subplot(4, 2, 4)
        plt.title('P')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_P)

        plt.subplot(4, 2, 5)
        plt.title('ES1')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_ES1)

        plt.subplot(4, 2, 6)
        plt.title('ES1S2')
        plt.xlabel("Time")
        plt.ylabel("Population")
        plt.plot(self.record_time, self.record_pop_ES1S2)

        plt.subplots_adjust(hspace=1)
        plt.show()


if __name__ == '__main__':
    sd = SimulationData()
    sd.update_propensities()   ### Initialize propensities ###
    sd.record_state()  ### Record initial state ###

    while sd.time < sd.end_time:
        tau = (1 / sd.a0) * np.log(1 / rnd.random())

        sd.time += tau
        sd.time_remainder += tau % sd.dt

        ### The following if blocks determine how many fixed time outputs must be skipped over (if any). ###
        if sd.time_remainder > sd.dt:
            sd.roll_over_boolean = True
            sd.time_remainder -= sd.dt

        if sd.roll_over_boolean:
            for i in range(int(tau / sd.dt) + 1):
                sd.fixed_time += sd.dt
                sd.record_state()  # Record state before reaction.
            roll_over_boolean = False
        else:
            for j in range(int(tau / sd.dt)):
                sd.fixed_time += sd.dt
                sd.record_state()  # Record state before reaction.

        sd.react()
        sd.update_propensities()

    sd.record_state()  # Records final state of the system.
    sd.display_results()


