import pylab as P
import random as rnd
import numpy as np

from scipy.stats import poisson

k1 = 0.1
k2 = 2.0
A = 0
dt = 0.005
n = 1000
time = 0
timeMax = 5000
data = [A]
timeSave = [0]

# Run until 1000 reactions have occurred. Time will be counted in
# steps of 0.005s. If two reactions occur in the same dt,
# only one reaction will be counted.

i = 1
while time < timeMax :
    time += dt
    c = 0

    if rnd.random() < k1*A*dt :
        c -= 1

    if rnd.random() < k2*dt :
        c += 1
   
    A += c

    data.append(A)
    timeSave.append(time)

P.subplot('211')
P.plot(timeSave,data)

P.subplot('212')
P.hold('on')

bins = np.arange(50) - 0.5
P.hist(data, bins=bins,range=None,normed=True,label="Label", align='mid')

bins = range(50)
rv = poisson(k2/k1)
P.plot(bins, rv.pmf(bins), 'rs')
P.show()

print(rv.pmf([0,1,2]))

print "Time elapsed:", time, "Final population:", A
