import numpy as np
import pprint


def gaussian_elimination(A, b):
		n = len(A)

		# We will be iterating down the diagonal of the matrix
		for k in range(n - 1):
				# row_idx is the row number of the largest absolute entry in th$
				row_idx = abs(A[k:, k]).argmax() + k

				# Swap row at row_idx with top row
				if row_idx != k:
						A[[k, row_idx]] = A[[row_idx, k]]
						b[[k, row_idx]] = b[[row_idx, k]]

				# Transform into upper-right triangle form
				for row in range(k + 1, n):
						# A[k, k] is the largest element in the column.
						# What fraction of A[k, k] is needed to eliminate the e$
						fraction = A[row, k] / A[k, k]

						# Eliminate A[k+1, k] and modify remaining row
						A[row, k:] = A[row, k:] - fraction * A[k, k:]
						b[row] = b[row] - fraction * b[k]

		# Solving for x
		x = np.zeros(n)
		x[n-1] = b[n-1] / A[n-1, n-1]  # Set the last element
		# From the second to last element to the first.
		for k in range(n - 2, -1, -1):
				x[k] = (b[k] - np.dot(A[k, k:], x[k:])) / A[k, k]
		return x


def mult_matrix(M, N):
	tuple_N = zip(*N)

	# Nested list comprehension to calculate matrix multiplication
	return [[sum(el_m * el_n for el_m, el_n in zip(row_m, col_n)) for col_n in tuple_N] for row_m in M]


A = np.array([[0.00001, 2.0, 6.0],
			  [4.0, 0.002, -1.0],
			  [-2.0, 3.0, 5.0]])
b = np.array([[1.0 / 3.0], [2.0 / 9.0], [5.0 / 7.0]])

print 'Beginning with A:'
pprint.pprint(A)

nx, ny = A.shape
n = nx

L = np.zeros((nx,ny))
U = np.zeros((nx,ny))

# Identity matrix
P = np.zeros((nx, ny))
for i in range(n):
	for j in range(n):
		if i == j:
			P[i,j] = 1.0
# Rearrange
for k in range(n):
	row_idx = abs(A[k:, k]).argmax() + k
	if k != row_idx:
		# Swap the rows
		P[[k, row_idx]] = P[[row_idx, k]]

PA = np.dot(P, A)

# Dolittle's method
for j in range(n):
	L[j, j] = 1.0
	for i in xrange(j+1):
		s1 = sum(U[k, j] * L[i, k] for k in range(i))
		U[i, j] = PA[i][j] - s1
	for i in xrange(j, n):
		s2 = sum(U[k, j] * L[i, k] for k in range(j))
		L[i, j] = (PA[i, j] - s2) / U[j, j]

# (1)
print 'L = \n' + str(L)
print 'U = \n' + str(U)
LU = np.dot(L, U)

# (2)
print 'L*U = P*A'
print 'L*U:'
pprint.pprint(LU)
print 'P*A:'
pprint.pprint(PA)

# (3), gaussian_elimination() is being reused from last homework set
Pb = np.dot(P, b)
y = gaussian_elimination(L, Pb)
x = gaussian_elimination(U, y)
print 'x:'
pprint.pprint(x)

# (4)
row1 = A[0, :]
res = row1[0] * x[0] + row1[1] * x[1] + row1[2] * x[2]
print 'b[1] = %10.10f' % res
if not abs(res - b[0]) > 1e-10:
	print 'Within error.'

row2 = A[1, :]
res = row2[0] * x[0] + row2[1] * x[1] + row2[2] * x[2]
print 'b[2] = %10.10f' % res
if not abs(res - b[1]) > 1e-10:
	print 'Within error.'

row3 = A[2, :]
res = row3[0] * x[0] + row3[1] * x[1] + row3[2] * x[2]
print 'b[0] = %10.10f' % res
if not abs(res - b[2]) > 1e-10:
	print 'Within error.'