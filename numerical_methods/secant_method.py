import numpy as np
from math import tan

def my_secant_method(f, a, b, delta=1e-6, iterations=6):
	# x is a list of increasingly accurate approximations
	x = np.zeros(iterations)

	# Place initial values
	x[0] = a
	x[1] = b

	# Fill our list of approximations
	for i in range(2, iterations):
		dx = x[i - 1] - x[i - 2]
		dy = f(x[i - 1]) - f(x[i - 2])

		x[i] = x[i - 1] - f(x[i - 1]) * dx / dy

	return x, dx


if __name__ == '__main__':

	# Define our mathematical function
	f = lambda x: x * tan(x) - 2
	# Define points
	a = 1.0
	b = 1.5

	# Calling!
	x, dx = my_secant_method(f, a, b)

	# Our root
	root = x[-1]

	# Printing results
	print 'Root obtained: ' + str(root) + ', with error: ' + str(dx)

	# Check validity of answer
	f0 = f(root)
	print 'f(' + str(root) + ') = ' + str(f0)
	if abs(f0) < abs(dx):
		print 'This is within uncertainty.'
	else:
		print 'This is outside uncertainty.'