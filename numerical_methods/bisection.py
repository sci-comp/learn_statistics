import numpy as np


def my_bisect(f, a, b, delta=1e-6):
	dx = b - a

	counter = 1
	while np.abs(dx) > delta:
		mp = (a + b) / 2  # mp = mid point between a and b
		if (f(a) * f(mp)) < 0:
			dx = mp - a
			b = mp
		else:
			dx = b - mp
			a = mp
		counter += 1
	return mp, dx, counter


if __name__ == '__main__':
	# Define our mathematical function
	f = lambda x: x * np.tan(x) - 2

	a = 0.0
	b = 1.3
	mp, dx, counter = my_bisect(f, a=a, b=b)

	print 'With a=' + str(a) + ', b=' + str(b)
	print 'Iteration Number: ' + str(counter)
	print 'Root obtained: ' + str(mp)
	print 'Estimated error: +/- ' + str(dx)
	print 'f(x=' + str(mp) + ') = ' + str(f(mp))
	print '======================================'

	a = 1.3
	b = 2.0
	mp, dx, counter = my_bisect(f, a=a, b=b)

	print 'With a=' + str(a) + ', b=' + str(b)
	print 'Iteration Number: ' + str(counter)
	print 'Root obtained: ' + str(mp)
	print 'Estimated error: +/- ' + str(dx)
	print 'f(x=' + str(mp) + ') = ' + str(f(mp))
