import numpy as np


def integral(n=9):
	f = np.zeros(n + 1)
	h = 1.0 / n

	# Filling points in our function
	for i in range(n + 1):
		x = h * i
		f[i] = np.exp(-x)

	s0 = s1 = s2 = 0.0

	for i in range(1, n, 2):
		s0 += f[i]
		s1 += f[i - 1]
		s2 += f[i + 1]

	s = (s1 + (4 * s0) + s2) / 3

	if (n+1) % 2 == 0:
		ans = h * (s + (5 * f[n] + 8 * f[n - 1] - f[n - 2]) / 12)
		return ans
	else:
		ans = h * s
		return ans


if __name__ == '__main__':
	ans = integral()
	print 'The integral of exp(-x) from x = 0 .. 1 is known to be (e-1)/e (~0.63212). '
	print 'Through the Simpson method (n=9), we found the value to be: ' + str(ans)
	print 'This is a difference of: ' + str(np.abs(0.63212-ans))